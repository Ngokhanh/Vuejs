@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Vuejs_trainning</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                     <div class="form-group">
                        <router-link :to="{name: 'createCompany'}" class="btn btn-success">Create new company</router-link>
                    </div>
                    <div style="overflow: auto;">
                        <router-view name="companiesIndex"></router-view>
                        <router-view></router-view>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
