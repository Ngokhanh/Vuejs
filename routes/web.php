<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/user', 'Usercontroller@index')->name('home');
Route::get('/home', function () {
    return view('home');
})->name('list');
Route::get('/delete/{id}', 'Usercontroller@delete')->name('delete');
Route::get('/create', 'Usercontroller@create')->name('create');
Route::post('/createpost', 'Usercontroller@createpost')->name('createpost');