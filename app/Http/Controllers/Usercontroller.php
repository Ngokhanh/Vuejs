<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class Usercontroller extends Controller
{
    public function index(){

    	$users =User::all();
    	return response()->json([
		    'user'=> $users
		]);
    }
    public function delete($id){

    	$delete=User::findOrFail($id);
    	$delete->delete();
    	return '';

    }
    public function create(){

    	return view('create');
    }
    public function createpost(Request $request){

	    	$name=$request->name;
	    	$email=$request->email;
	    	$password=Hash::make($request->password);
	    	$users = User::insert(['name'=>$name,'email'=>$email,'password'=>$password]);
	        return response()->json(['user'=> $users]);
    }
}
